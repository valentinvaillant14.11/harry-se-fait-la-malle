# -*- coding: utf-8 -*-
"""
Created on Fri Dec 17 12:37:34 2021

@author: VAILLANT-GUILLOT
"""

#Ollivander, fabriquant de baguettes magiques

def ollivander() : 
    monnaie_gallions = int(input("Combien de gallions dois-je vous rendre ?"))
    monnaie_mornilles = int(input("Combien de mornilles dois-je vous rendre ?"))
    monnaie_noises = int(input("Combien de noises dois-je vous rendre ?"))
    if monnaie_noises > 0: 
       print("Je vous rends {monnaie_noises} noises")
    if monnaie_mornilles >= 29:
       print("Je vous rends {monnaie_mornilles} mornilles")
    if monnaie_gallions >= 17:
       print("Je vous rends {monnaie_gallions} gallions")
       