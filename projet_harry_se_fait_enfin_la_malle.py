# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

fournitures_scolaires = [{'Nom' : 'Manuel scolaire', 'Poids' : 0.55, 'Mana' : 11},
{'Nom' : 'Baguette magique', 'Poids' : 0.085, 'Mana' : 120},
{'Nom' : 'Chaudron', 'Poids' : 2.5, 'Mana' : 2},
{'Nom' : 'Boîte de fioles', 'Poids' : 1.2, 'Mana' : 4},
{'Nom' : 'Téléscope', 'Poids' : 1.9, 'Mana' : 6},
{'Nom' : 'Balance de cuivre', 'Poids' : 1.3, 'Mana' : 3},
{'Nom' : 'Robe de travail', 'Poids' : 0.5, 'Mana' : 8},
{'Nom' : 'Chapeau pointu', 'Poids' : 0.7, 'Mana' : 9},
{'Nom' : 'Gants', 'Poids' : 0.6, 'Mana' : 25},
{'Nom' : 'Cape', 'Poids' : 1.1, 'Mana' : 13}]


# Remplir la malle n'importe comment sans dépasser le poids maximal supporté par la malle
def remplir_malle(fournitures):
    poids_max_malle = 4
    malle = []
    for objet in fournitures:
        if objet['Poids'] <= poids_max_malle:
            malle.append(objet)
            poids_max_malle = poids_max_malle - objet['Poids']
    return malle
        
# Calculer le poids final de la malle
def calcul_poids(malle):
    poids_malle = 0
    for objet in malle:
        poids_malle = poids_malle + objet['Poids']
    return poids_malle

# Trier tous les objets des fournitures scolaires par poids dans l'ordre décroissant
def tri_poids(fournitures):
    for i in range (1, len(fournitures)):
       while fournitures[i]['Poids'] > fournitures[i - 1]['Poids'] and i > 0:
        fournitures[i], fournitures[i - 1] = fournitures[i - 1], fournitures[i]
        i = i - 1
    return fournitures

# Trier tous les objets des fournitures scolaires par mana dans l'ordre décroissant
def tri_mana(fournitures):
    for i in range (1, len(fournitures)):
        while fournitures[i]['Mana'] > fournitures[i - 1]['Mana'] and i > 0:
         fournitures[i], fournitures[i - 1] = fournitures[i - 1], fournitures[i]
         i = i - 1
    return fournitures

# Calculer la charge magique final de la malle
def calcul_mana(malle):
    mana_malle = 0
    for objet in malle:
        mana_malle = mana_malle + objet['Mana']
    return mana_malle
           
malle_a = remplir_malle(fournitures_scolaires)
print(malle_a)        
            
poids_malle_a = calcul_poids(malle_a)
print(poids_malle_a)
 

tri_fournitures_b = tri_poids(fournitures_scolaires)
print(tri_fournitures_b)

malle_b = remplir_malle(fournitures_scolaires)
print(malle_b)

poids_malle_b = calcul_poids(malle_b)
print(poids_malle_b)


tri_fournitures_c = tri_mana(fournitures_scolaires)
print(tri_fournitures_c)
    
malle_c = remplir_malle(fournitures_scolaires) 
print(malle_c)
  
poids_malle_c = calcul_mana(fournitures_scolaires)
print(poids_malle_c)

   

