# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from tkinter import *

#creer une première fenetre
window = Tk()



#personnaliser cette fenetre
window.title("Chemin de Traverse")
window.geometry("800x600")
window.minsize(480, 360)
window.config(background='black')
frame = Frame(window, bg='black', bd=1, relief=SUNKEN)

#ajouter un texte
label_title = Label(frame, text="Bienvenue sur le Chemin de Traverse", font=('Algerian', 30), bg="black", fg='white')
label_title.pack()

#ajouter autre texte
label_subtitle = Label(frame, text="Où souhaitez-vous aller ?", font=('Algerian', 20), bg="black", fg='white')
label_subtitle.pack()

#ajouter un bouton
yt_button = Button(frame, text="Entrer chez Fleury & Bott", font=('Algerian', 15), bg='black', fg='white')
yt_button.pack(pady=25)

#ajouter un deuxième boutton
yt_button = Button(frame, text="Entrer chez Madame Guipire", font=("Algerian", 15), bg="black", fg="white")
yt_button.pack(pady=25)

#ajouter un troisième boutton
yt_button = Button(frame, text="Entrer chez Ollivander", font=("Algerian", 15), bg="black", fg="white")
yt_button.pack(pady=25)

#ajouter un boutton quitter
yt_button = Button(frame, text="Quitter", font=("Algerian", 15), bg="black", fg="white", command=window.destroy)
yt_button.pack(pady=25)

frame.pack(expand=YES)






window.mainloop()

